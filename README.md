# Bienvenido


[![pipeline status](https://gitlab.com/untref-ingsoft/modelo-de-repositorio/badges/main/pipeline.svg)](https://gitlab.com/untref-ingsoft/modelo-de-repositorio/commits/main)

## Instrucciones iniciales
Este el es repositorio que utilizaras durante la materia para realizar tus tareas.

Debes comenzar por modificar este archivo:

* Donde dice "Bienvenido" coloca tu nombre completo
* Ajusta la linea que refiere al "pipeline status" para que refleje el estado de tu proyecto en lugar del proyecto base
* Luego de los dos ajustes anteriores debes eliminar todas la lineas de las "Intrucciones iniciales"

¡Suerte!
